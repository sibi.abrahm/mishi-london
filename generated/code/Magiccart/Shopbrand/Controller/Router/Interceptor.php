<?php
namespace Magiccart\Shopbrand\Controller\Router;

/**
 * Interceptor class for @see \Magiccart\Shopbrand\Controller\Router
 */
class Interceptor extends \Magiccart\Shopbrand\Controller\Router implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\ActionFactory $actionFactory, \Magento\Framework\App\ResponseInterface $response, \Magiccart\Shopbrand\Model\ShopbrandFactory $brand, \Magiccart\Shopbrand\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($actionFactory, $response, $brand, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'match');
        return $pluginInfo ? $this->___callPlugins('match', func_get_args(), $pluginInfo) : parent::match($request);
    }
}
