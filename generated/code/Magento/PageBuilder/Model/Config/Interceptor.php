<?php
namespace Magento\PageBuilder\Model\Config;

/**
 * Interceptor class for @see \Magento\PageBuilder\Model\Config
 */
class Interceptor extends \Magento\PageBuilder\Model\Config implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\PageBuilder\Model\Config\CompositeReader $reader, \Magento\Framework\Config\CacheInterface $cache, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, $cacheId = 'pagebuilder_config')
    {
        $this->___init();
        parent::__construct($reader, $cache, $scopeConfig, $cacheId);
    }

    /**
     * {@inheritdoc}
     */
    public function getMenuSections() : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMenuSections');
        return $pluginInfo ? $this->___callPlugins('getMenuSections', func_get_args(), $pluginInfo) : parent::getMenuSections();
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypes() : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getContentTypes');
        return $pluginInfo ? $this->___callPlugins('getContentTypes', func_get_args(), $pluginInfo) : parent::getContentTypes();
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled() : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEnabled');
        return $pluginInfo ? $this->___callPlugins('isEnabled', func_get_args(), $pluginInfo) : parent::isEnabled();
    }

    /**
     * {@inheritdoc}
     */
    public function isContentPreviewEnabled() : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isContentPreviewEnabled');
        return $pluginInfo ? $this->___callPlugins('isContentPreviewEnabled', func_get_args(), $pluginInfo) : parent::isContentPreviewEnabled();
    }

    /**
     * {@inheritdoc}
     */
    public function merge(array $config)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'merge');
        return $pluginInfo ? $this->___callPlugins('merge', func_get_args(), $pluginInfo) : parent::merge($config);
    }

    /**
     * {@inheritdoc}
     */
    public function get($path = null, $default = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'get');
        return $pluginInfo ? $this->___callPlugins('get', func_get_args(), $pluginInfo) : parent::get($path, $default);
    }

    /**
     * {@inheritdoc}
     */
    public function reset()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'reset');
        return $pluginInfo ? $this->___callPlugins('reset', func_get_args(), $pluginInfo) : parent::reset();
    }
}
